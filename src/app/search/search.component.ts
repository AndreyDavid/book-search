import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';
import { Books } from '../books.model';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(private searchService: SearchService) { }

  searchTerm ='';
  searchRes : Books;
  searchError = '';
  dispError = false;
  user = sessionStorage.getItem('user');
  bookList:{id:number,title:string}[] = [];

  modalInfo = { title:'string',
                  numPages:0,
                  authors:['string'],
                  publishedDate:'string'} 

  showModal = false;
  
  ngOnInit() {
    
  }

  
  async onSearchChange(){
    
    console.log(this.searchTerm);
    let searchVal=this.searchTerm;

    if(searchVal.length>0){
      
      if(searchVal.split(" ").length>0){
        searchVal = searchVal.split(" ").join('+');
      }
      await this.searchService
                .booklist(searchVal)
                .then(res =>{
                  this.searchRes=res;
                  this.bookList=this.searchService.createList(this.searchRes);
                })
                .catch(err=>{
                  this.searchError = err;
                  this.dispError = true;
                });
      }
  }

  onClickedBook(bookTitle:string){
    this.showModal = true;
    Object.assign(this.modalInfo,this.searchService.bookDataToDisp(bookTitle,this.searchRes))
  }

  addToWishlist(title:string){
    this.searchService.addWish(title);
    this.showModal= false;
  }
}
