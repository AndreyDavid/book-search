//model for google books API
export class Books{
    
      kind: string;
      totalItems: number;
      items: {
          kind:string,
          id:string,
          etag:string,
          selfLink: string,
          volumeInfo:{
              title:string,
              subtitle:string,
              authors:[],
              publisher:[],
              publishedDate:string,
              description:""
              industryIdentifiers:{}[],
              readingModes:{},
              pageCount: number,
              printType: string,
              categories:string[],
              maturityRating:string,
              allowAnonLogging:boolean,
              contentVersion:string,
              panelizationSummary:{},
              imageLinks:{
                smallThumbnail:string,
                thumbnail:string
              },
              language:string
          }
      }[]  
}



