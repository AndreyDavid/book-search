import { Component, OnInit } from '@angular/core';
import { SearchService } from './search.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(private searchService:SearchService){}

  title  = 'Book Search App';
  show = false;
  ngOnInit(){
  
  }
  showStatus(){
     this.show = this.searchService.showToolbar();

  }

}
