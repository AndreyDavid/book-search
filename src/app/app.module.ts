import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { HomeComponent } from './home/home.component';

import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


const appRoutes: Routes = [
  {path: ''        , component: HomeComponent},
  {path: 'search'  , component:SearchComponent},
  {path: 'wishlist', component: WishlistComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    WishlistComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
