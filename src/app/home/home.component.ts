import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  //to disp error MSG
  errorDisp = false;
  errorMsg = '';
  
  constructor(private searchService: SearchService) { }

  ngOnInit() {
  }

  // goes on when the submit button is clicked
  onSubmit(form: NgForm){
    let name = form.value.userName;

    //check if empty
    if (name.length===0){
      this.errorDisp = true;
      this.errorMsg  = 'The user name field is empty please enter your name';
    }else{
      sessionStorage.setItem('user', name);
      window.location.href = 'http://localhost:4200/search';
    }
  }
}
