import { Injectable, ComponentFactoryResolver } from "@angular/core";
import { HttpClient,HttpParams } from '@angular/common/http';
import { Books } from './books.model';


@Injectable({providedIn: 'root'})
export class SearchService{

constructor(private http: HttpClient){}

apikey = 'AIzaSyC9HXxW7clcaWrMzVxaTj8Jp_2ycC8fvoc';    
// wishlist:string[];
endPoint = 'https://www.googleapis.com/books/v1/volumes';


users:string[]=[];
wishlist:string[] =[];
movedisable = true;
additionalInfo={ title:'string',
                 imgUrl:'string', 
                 numPages:0,
                 authors:['string'],
                 publishedDate:'string'} ;       






//returns list of relevant book names
booklist(searchVal:string):Promise<Books>{

    let params = new HttpParams();
    params= params.append('q',searchVal);
    params = params.append('maxResults','20');
    params = params.append('key',this.apikey);

    return this.http
                .get<Books>(this.endPoint,{params})
                .toPromise();
    }

createList(searchRes:Books){
    
    let cnt: number;
    let list:{id:number,title:string}[] =[];

    if(searchRes.items.length ===0){
        return list;
    }else if(searchRes.items.length>0 && searchRes.items.length<20){
        cnt = searchRes.items.length;
    }else{
        cnt =20;
    }

    for(let i=0;i<cnt;i++){
        list.push({id: i,
                   title: searchRes.items[i].volumeInfo.title})    
    }
    return list;

}
// additional data to display on modal
bookDataToDisp(bookTitle:string,searchRes:Books){
    let found = false;
    let i = 0;

    while(!found && i<40){
        if(bookTitle===searchRes.items[i].volumeInfo.title){
            found = true;
            this.additionalInfo.title = bookTitle;
            this.additionalInfo.imgUrl =searchRes.items[i].volumeInfo.imageLinks.smallThumbnail;
            this.additionalInfo.numPages =searchRes.items[i].volumeInfo.pageCount;
            this.additionalInfo.authors =searchRes.items[i].volumeInfo.authors;
            this.additionalInfo.publishedDate =searchRes.items[i].volumeInfo.publishedDate;
            
            return this.additionalInfo;
        
        }else{
            i+=1;
            }
        }
    }
// adds element to wishlist
addWish(title:string){
        if(!this.wishlist){
            this.wishlist.push(title);
        }else if (this.wishlist.filter(x=>x===title).length===0 ){
            this.wishlist.push(title);
        }
        
    }
//removes element from wish list
removeWish(title:string){
    this.wishlist = this.wishlist.filter(a=>a!==title);
}
//checks if there is a username present in local storage
showToolbar(){
    let user = sessionStorage.getItem('user');

    if(user===null){
        return false;
    }else{
        return true;
    }
}
}
