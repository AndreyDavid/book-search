import { Component, OnInit } from '@angular/core';
import { SearchService } from '../search.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.css']
})
export class WishlistComponent implements OnInit {

  constructor(private searchService: SearchService) { }

  ngOnInit() {

  }

  onDelWish(title:string){
    this.searchService.removeWish(title);
  }
}
